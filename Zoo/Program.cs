﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;



namespace Zoo
{
    public enum ZooFieldType { Cage, OpenField, Aquarium, MonkeyHouse };
    class Program
    {
        static void Main(string[] args)
        {
            Load();

            DoDB();

            ZooFieldList.Inventory();

            UIMainMenu();

            

        }

        private static void DoDB()
        {
            using (var db = new ZooDbContext())
            {

                db.DBSet_ZooSpecies.Add(ZooSpeciesList.GetASpecies("Lion"));
                db.DBSet_ZooSpecies.Add(ZooSpeciesList.GetASpecies("Shark"));
                db.DBSet_ZooSpecies.Add(ZooSpeciesList.GetASpecies("Gaselle"));

                db.SaveChanges();

                // Display all Blogs from the database 
                var query = from b in db.DBSet_ZooSpecies
                            orderby b.SpeciesName
                            select b;

                Console.WriteLine("Species in the Zoo:");
                foreach (Species species in query)
                {
                    Console.WriteLine(species.SpeciesName);
                }

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            } 

        }

        private static void UIMainMenu()
        {
            Console.WriteLine("\n(A) - New animal");
            Console.WriteLine("(B) - Inventory");
            Console.WriteLine("(Z) - End");

            string str = Console.ReadLine();

            switch (str)
            {
                case "a":
                case "A":
                    UINewAnimal();
                    ZooFieldList.Inventory();
                    UIMainMenu();
                    break;

                case "b":
                case "B":
                    ZooFieldList.Inventory();
                    UIMainMenu();
                    break;

                case "z":
                case "Z":
                    break;

                default:
                    break;
            }
        }
        
        private static void UINewAnimal()
        {
            Console.WriteLine("\nNew Animal Menu");
            int counter = 0;
            foreach (Species species in ZooSpeciesList.SpeciesList)
            {
                counter++;
                Console.WriteLine( counter + " - " + species.GetSpeciesName());    
            }
            
            Console.WriteLine("\n0 - Main Menu");

            string strSpeciesNbInList = Console.ReadLine();

            if( strSpeciesNbInList == "0")
            {
                UIMainMenu();
                return;
            }

            string strSpeciesName = ZooSpeciesList.SpeciesList[ Convert.ToInt32( strSpeciesNbInList) -1 ].GetSpeciesName();

            UISetField(strSpeciesName);
        }

        private static void UISetField(string aSpeciesName)
        {
            Console.WriteLine("Name of the animal: ");
            string strAnimalName = Console.ReadLine();

            Console.WriteLine("Birthday of the animal: ");
            string strBirthday = Console.ReadLine();


            Animal animal = CreateAnimal( strAnimalName, aSpeciesName, strBirthday);


            if (ZooFieldList.TryAccomodate(animal) == false)
            {
                
                Species species = ZooSpeciesList.GetASpecies(aSpeciesName);
                if( species == null)
                {
                    Console.WriteLine("Error 51: unknown species.");
                    return;
                }

                Console.WriteLine("A new " + species.GetSuitableFieldType() + " is needed. Max number or animal in the"
                    + species.GetSuitableFieldType() + ":");

                string strNum = Console.ReadLine();

                ZooFieldList.AddToList(new Field(Convert.ToInt32(strNum), species.GetSuitableFieldType()));

                if (ZooFieldList.TryAccomodate(new Animal(strAnimalName, aSpeciesName, strBirthday)) == false)
                {
                    Console.WriteLine("Error 52: no place in the empty " + species.GetSuitableFieldType() + ".");
                }               
            }
        }

        private static Animal CreateAnimal(string strAnimalName, string aSpeciesName , string strBirthday)
        {
            Animal animal;

            if (aSpeciesName == "Chimpanzee")
            {
                Chimpanzee Chimpanzee = new Chimpanzee(strAnimalName, aSpeciesName, strBirthday);
                animal = (Animal)(Chimpanzee);
                return animal;
            }

            if (aSpeciesName == "Shark")
            {
                Shark shark = new Shark(strAnimalName, aSpeciesName, strBirthday);
                animal = (Animal)(shark);
                return animal;
            }
            
            if (aSpeciesName == "Antilope")
            {
                Antilope antil = new Antilope(strAnimalName, aSpeciesName, strBirthday);
                animal = (Animal)(antil);
                return animal;
            }
            
            animal = new Animal(strAnimalName, aSpeciesName, strBirthday);            
            return animal;
        }


        private static void Load()
        {

            // lion, antilope, gaselle

            Species SpeciesLion = CreateSpeciesForZooSpeciesList("Lion",true);
            Animal Leo = CreateAnimal("Leo", SpeciesLion.GetSpeciesName(), "2014,01,30");

            Species SpeciesAntilope = CreateSpeciesForZooSpeciesList("Antilope", false);            
            Animal Anti = CreateAnimal("Anti", SpeciesAntilope.GetSpeciesName(), "2012,01,01");

            Species SpeciesGaselle = CreateSpeciesForZooSpeciesList("Gaselle", false);
            Animal Gas = CreateAnimal("Gas", SpeciesGaselle.GetSpeciesName(), "2013,03,3");


            Field Cage1 = new Field(2, ZooFieldType.Cage);
            Field OpenField1 = new Field(6, ZooFieldType.OpenField);            

            ZooFieldList.AddToList(Cage1);
            ZooFieldList.AddToList(OpenField1);

            ZooFieldList.TryAccomodate(Leo);
            ZooFieldList.TryAccomodate(Anti);
            ZooFieldList.TryAccomodate(Gas);



            // shark
            Species SpeciesShark = CreateSpeciesForZooSpeciesList("Shark", true);
            Animal Shakira = CreateAnimal("Shakira", SpeciesShark.GetSpeciesName(), "1955,05,11");
            Aquarium Aquarium1 = new Aquarium(2, ZooFieldType.Aquarium);
            ZooFieldList.AddToList(Aquarium1);
            ZooFieldList.TryAccomodate(Shakira);



            // Chimpanzee
            Species SpeciesChimpanzee = CreateSpeciesForZooSpeciesList("Chimpanzee", true);
            Animal Chi = CreateAnimal("Chi", SpeciesChimpanzee.GetSpeciesName(), "1999,04,01");
            
            MonkeyHouse MonkeyHouse1 = new MonkeyHouse(4, ZooFieldType.MonkeyHouse);
            ZooFieldList.AddToList(MonkeyHouse1);
            ZooFieldList.TryAccomodate(Chi);


            // test interface
            Console.WriteLine("\nInterface test");
            IEat eat = Chi;
            IClimb climb = (Chimpanzee)(Chi);
            eat.Eating();
            climb.Climbing();
        }

        private static Species CreateSpeciesForZooSpeciesList(string aSpeciesname, bool aIsCarnevous)
        {
            Species newSpecies = new Species(aSpeciesname, aIsCarnevous);
            ZooSpeciesList.AddToList(newSpecies);
            return newSpecies;
        }
    }
}