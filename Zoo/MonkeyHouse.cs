﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    class MonkeyHouse:Field
    {
        protected int _nbOfClimbingBar;

        public MonkeyHouse(int aMaxNbOfAnimals, ZooFieldType aPlaceType)
            :base(aMaxNbOfAnimals , aPlaceType)
        {
            _nbOfClimbingBar = 15;
        }

        public int GetNbOfClimbingBar()
        {
            return _nbOfClimbingBar;
        }

        public bool SetNbOfClimbingBar(int aNbOfClimbingBar)
        {
            _nbOfClimbingBar = aNbOfClimbingBar;
            return true;
        }
    }
}
