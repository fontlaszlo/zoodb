﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    class Gaselle:Animal
    {

        protected static double _defaultSpeed = 42.9;
        protected double _speed;

        public Gaselle (string aAnimalName, string aSpeciesName, string aStrBirthday, double aspeed)
            : base( aAnimalName,  aSpeciesName,  aStrBirthday )
        {
            _speed = aspeed;
        }

        public Gaselle(string aAnimalName, string aSpeciesName, string aStrBirthday)
            : base(aAnimalName, aSpeciesName, aStrBirthday)
        {
            _speed = _defaultSpeed;
        }

        public bool SetSpeed(double aspeed)
        {
            _speed = aspeed;
            return true;
        }

        public double GetSpeed()
        {
            return _speed;
        }
    }
}
