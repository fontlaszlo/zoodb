﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    class Chimpanzee:Animal, IClimb
    {
         public Chimpanzee(string aAnimalName, string aSpeciesName, string aStrBirthday)
            : base(aAnimalName, aSpeciesName, aStrBirthday)
        { }

        void IClimb.Climbing()
        {
            Console.WriteLine("Chimpanzee is a good climber.");
        }
    }
}
