﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    class Shark : Animal, ISwim
    {

        protected static int _defaultNBOfTeeth = 250;
        protected int _nBOfTeeth;

        public Shark(string aAnimalName, string aSpeciesName, string aStrBirthday, int aNBOfTeeth)
            : base( aAnimalName,  aSpeciesName,  aStrBirthday )
        {
            _nBOfTeeth = aNBOfTeeth;
        }

        void ISwim.Swimming()
        {
            Console.WriteLine("Shark is a good swimmer");
        }

        public Shark(string aAnimalName, string aSpeciesName, string aStrBirthday)
            : base(aAnimalName, aSpeciesName, aStrBirthday)
        {
            _nBOfTeeth = _defaultNBOfTeeth;
        }

        public bool SetNbOfTeeth(int aNBOfTeeth)
        {
            _nBOfTeeth = aNBOfTeeth;
            return true;
        }

        public int GetNbOfTeeth()
        {
            return _nBOfTeeth;
        }
    }
  
}
