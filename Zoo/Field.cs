﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Field
    {
        static protected int _lastID = 0;
        protected int _id = 0;
        protected ZooFieldType _placeType;

        protected List<Animal> _animalsInOnePlace;
        protected int _maxNbOfAnimals;
        protected int _nbOfAnimals;
        protected int _widthCm;
        protected int _lengthCm;

        public Field(int aMaxNbOfAnimals, ZooFieldType aPlaceType)
        {
            if (aMaxNbOfAnimals < 1 || aMaxNbOfAnimals > 100 )
            {
                Console.WriteLine("Error 10: Max Nb Of Animals is out of range.");
                return;
            }
            _widthCm = 100 * 100;
            _lengthCm = 200 * 100;
            _placeType = aPlaceType;
            _maxNbOfAnimals = aMaxNbOfAnimals;
            _id = _lastID++;
            _animalsInOnePlace = new List<Animal>();

            Console.WriteLine("New " + aPlaceType + "  created. Field ID " + _id);
        }

        public bool IsNotFull()
        {
            if (_nbOfAnimals < _maxNbOfAnimals)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetFreePlaces()
        {
            return _maxNbOfAnimals - _nbOfAnimals;
        }

        public int GetMaxPlaces()
        {
            return _maxNbOfAnimals;
        }

        public int GetNbOfAnimals()
        {
            return _nbOfAnimals;
        }

        public int GetPlaceID()
        {
            return _id;
        }

        public ZooFieldType GetPlaceType()
        {
            return _placeType;
        }

        public bool TryPut(Animal aAnimal, ZooFieldType aPlaceType)
        {
            if (_placeType == aPlaceType)
            {
                if (_maxNbOfAnimals > _nbOfAnimals)
                {
                    _animalsInOnePlace.Add(aAnimal);
                    _nbOfAnimals++;
                    Console.WriteLine(aAnimal.GetAnimalName() + " ID " + aAnimal.GetAnimalID() + " PLACED in field ID " + _id.ToString() + " type: " + aPlaceType);
                    return true;
                }
            }

            return false;
        }

        public void FieldInventory()
        {
            foreach (var item in _animalsInOnePlace)
            {
                Console.WriteLine(item.GetAnimalName() + " " + item.GetBirthday() + " " + " (" + item.GetSpeciesName() + ") ");
            }
        }

        public int GetLengthCm()
        {
            return _lengthCm;
        }

        public int GetWidthCm()
        {
            return _widthCm;
        }

        public bool SetLengthCm(int aLengthCm)
        {
            _lengthCm = aLengthCm;
            return true;
        }

        public bool SetWidthCm(int aWidthCm)
        {
            _widthCm = aWidthCm;
            return true;
        }

        public Field Get()
        {
            return this;
        }
    }
}
