﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public class Animal : Species, IEat
    {
        protected static int _lastId = 0;

        protected int _id;
        protected DateTime _birthDay;
        protected string _animalName;

        public Animal(string aAnimalName, string aSpeciesName, string aStrBirthday)
            : base(aSpeciesName, ZooSpeciesList.GetASpecies(aSpeciesName).IsCarnevorous())
        {
            Species species = ZooSpeciesList.GetASpecies(aSpeciesName);
            if (species != null)
            {
                this.SetBirthDay(aStrBirthday);
                _animalName = aAnimalName;
                _id = _lastId++;

                Console.WriteLine("Animal  " + aAnimalName + " defined. ID: " + _id.ToString());

            }
            else
            {
                Console.WriteLine("Error 7. Animal  " + aAnimalName + " not defined.");
            }
        }
        void IEat.Eating()
        {
            Console.WriteLine("Animal eats twice a day");
        }

        public int GetAnimalID()
        {
            return _id;
        }

        public string GetAnimalName()
        {
            return _animalName;
        }

        public bool SetBirthDay(string aStrBirthday)
        {
            _birthDay = Convert.ToDateTime(aStrBirthday);
            return true;
        }

        public DateTime GetBirthday()
        {
            return _birthDay;
        }
    }
}
