﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    static class ZooFieldList
    {

        public static List<Field> FieldList;

        static ZooFieldList()
        {
            FieldList = new List<Field>();
        }

        public static Field GetFieldByID(int aId)
        {
            foreach (Field field in FieldList)
            {
                if (field.GetPlaceID() == aId)
                {
                    return field;
                }
            }

            Console.WriteLine("Error 8: unknown field ID: " + aId);
            return null;
        }

        public static bool AddToList(Field aField)
        {
            if (FieldList != null)
            {
                foreach (Field field in FieldList)
                {
                    if (field.GetPlaceID() == aField.GetPlaceID())
                    {
                        Console.WriteLine("Error 2. Fields " + aField.GetPlaceID() + " " + aField.GetPlaceType() + " already defined.");
                    }
                }
            }

            FieldList.Add(aField);
            Console.WriteLine("Field " + aField.GetPlaceID() + " " + aField.GetPlaceType() + " added to ZooFields.");
            return true;
        }

        public static bool TryAccomodate(Animal aAnimal)
        {
            foreach (Field field in FieldList)
            {
                 if (field.TryPut(aAnimal, aAnimal.GetSuitableFieldType()))
                {
                    return true;
                }
            }

            Console.WriteLine("no place for " + aAnimal.GetSpeciesName() + ". Please create a new field.");
            return false;
        }

        public static void Accomodate(Animal aAnimal)
        {
            if ( TryAccomodate( aAnimal ) == false)
            {
                    ZooFieldList.AddToList(new Field(2, aAnimal.GetSuitableFieldType()));                   
            }
            
            if (TryAccomodate(aAnimal) == false)
            {
                Console.WriteLine("Error 9: no place for " + aAnimal.GetSpeciesName() + ".");
            }
        }
        
        public static void Inventory()
        {
            Console.WriteLine("\nZoo Inventory");
            foreach (Field field in FieldList)
            {
                Console.WriteLine("------------------");
                Console.WriteLine( field.GetPlaceType() + " " + field.GetMaxPlaces() + "/" + field.GetNbOfAnimals() + ":");
                field.FieldInventory();
            }
        }
    }
}
