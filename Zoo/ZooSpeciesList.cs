﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    public static class ZooSpeciesList
    {
        public static List<Species> SpeciesList;

        static ZooSpeciesList()
        {
            SpeciesList = new List<Species>();
        }

        public static bool AddToList(Species aSpecies)
        {
            if (SpeciesList != null)
            {
                foreach (var item in SpeciesList)
                {
                    if (item.GetSpeciesName() == aSpecies.GetSpeciesName())
                    {
                        Console.WriteLine("Error 2. Species " + aSpecies.GetSpeciesName() + " already defined.");
                    }
                }
            }

            SpeciesList.Add(aSpecies);
            Console.WriteLine("Species " + aSpecies.GetSpeciesName() + " added to SpeciesList.");
            return true;
        }

        public static Species GetASpecies(string aSpeciesName)
        {
            if (SpeciesList != null)
            {
                foreach (var item in SpeciesList)
                {
                    if (item.GetSpeciesName() == aSpeciesName)
                    {
                        return item;
                    }
                }
            }

            Console.WriteLine("Error 3. " + aSpeciesName + " Not defined Species.");
            return null;
        }
    }
}
