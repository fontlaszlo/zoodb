﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Zoo
{
    public class Species
    {
        [Key]
        public string SpeciesName { get; set; }
        protected bool _isCarnivorous;

        public Species(string aName, bool aIsCarnivorous)
        {
            SpeciesName = aName;
            _isCarnivorous = aIsCarnivorous;
            Console.WriteLine("Species " + aName + "  defined.");
        }

        public string GetSpeciesName()
        {
            return SpeciesName;
        }

        public bool IsCarnevorous()
        {
            return _isCarnivorous;
        }

        public Species GetSpecies()
        {
            return this;
        }
        
        public ZooFieldType GetSuitableFieldType()
        {
            if (SpeciesName == "Shark")
            {
                return ZooFieldType.Aquarium;
            }

            if (SpeciesName == "Chimpanzee")
            {
                return ZooFieldType.MonkeyHouse;
            }

            if (_isCarnivorous)
            {
                return ZooFieldType.Cage;
            }

            return ZooFieldType.OpenField;
        }
    }

}
