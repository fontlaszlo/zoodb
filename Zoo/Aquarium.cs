﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zoo
{
    class Aquarium :Field
    {
        protected int _heightCm;

        public Aquarium(int aMaxNbOfAnimals, ZooFieldType aPlaceType)
            :base(aMaxNbOfAnimals , aPlaceType)
        {
            _heightCm = 500;
        }

        public int GetHeightCm()
        {
            return _heightCm;
        }

        

        public bool SetHeightCm(int aHeightCm)
        {
            _widthCm = aHeightCm;
            return true;
        }
    }
}
