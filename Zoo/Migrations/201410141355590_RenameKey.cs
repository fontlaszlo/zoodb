namespace Zoo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameKey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Species");
            AddColumn("dbo.Species", "SpeciesName", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Species", "SpeciesName");
            DropColumn("dbo.Species", "_speciesName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Species", "_speciesName", c => c.String(nullable: false, maxLength: 128));
            DropPrimaryKey("dbo.Species");
            DropColumn("dbo.Species", "SpeciesName");
            AddPrimaryKey("dbo.Species", "_speciesName");
        }
    }
}
